package keyvaluestore;

/**
 * A Simple Key Value Store interface with get and put methods
 * 
 * @author Dhandapani
 * @param <K>
 * @param <V>
 */
public interface IKeyValueStore<K, V> {
	/**
	 * Gets the Value of type V for the given key of type K
	 *  
	 * @param key
	 * @return
	 */
	public V get(K key);
	
	/**
	 * Adds the Given Key and Value to Store.
	 * @param key
	 * @param value
	 */
	public void put(K key, V value);
}
