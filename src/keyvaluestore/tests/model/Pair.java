package keyvaluestore.tests.model;

import java.io.Serializable;

public class Pair<F extends Serializable, S extends Serializable> implements Serializable{
	private F first;
	private S second;
	
	public Pair(F first, S second) {
		this.first = first;
		this.second = second;
	}
	
	public F getFirst() {
		return first;
	}
	public void setFirst(F first) {
		this.first = first;
	}
	public S getSecond() {
		return second;
	}
	public void setSecond(S second) {
		this.second = second;
	}
	
	@Override
	public boolean equals(Object obj) {
		Pair object = (Pair) obj;
		if(object == null) {
			return false;
		}
		
		if((this.getFirst() != null && object.getFirst() == null) && (this.getFirst() == null && object.getFirst() != null) && !this.getFirst().equals(object.getFirst())) {
			return false;
		}
		
		if((this.getSecond() != null && object.getSecond() == null) && (this.getSecond() == null && object.getSecond() != null) && !this.getSecond().equals(object.getSecond())) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public int hashCode() {
		int hashCode = 13;
		hashCode += 31 * (this.first != null? this.first.hashCode() : 0);
		hashCode += 37 * (this.second != null? this.second.hashCode() : 0);
		
		return hashCode;
	}
}
